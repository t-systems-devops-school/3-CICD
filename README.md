## Homework 1
*[#homework]() [#cicd1]()*

#### Create a first dummy pipeline:
- Login to gitlab.
- Import [course project](https://gitlab.com/t-systems-devops-school/course-project) into your personal gitlab space.
- Create a pipeline with 3 stages: `build - test - deploy` with dummy `echo` scripts jobs.
- Send screenshots with homework hashtags that contain the following information:
  - content of `.gitlab-ci.yml` file
  - working pipeline

#### Useful links
- [Gitlab docs](https://docs.gitlab.com)
- [Registering Gitlab Runner](https://docs.gitlab.com/runner/register/index.html)
- [Import project to Gitlab by URL](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html)
- [.gitlab-ci.yml file](https://docs.gitlab.com/ci/quick_start/)

## Homework 2-1
*[#homework]() [#cicd2_1]()*

#### Update your pipeline to build your applications within it:
- Add new build jobs to `.gitlab-ci.yml` to build your applications without dockerization.
- Keep built `.jar` files as job artifacts with 1 hour expiration time.
- Send screenshots with homework hashtags that contain the following information:
  - content of `.gitlab-ci.yml` file
  - saved artifacts

#### Useful links
- [Gitlab job artifacts](https://docs.gitlab.com/ci/jobs/job_artifacts/)

## Homework 2-2
*[#homework]() [#cicd2_2]()*

#### Use cache to speed up the build process:
- Configure your build jobs to keep gradle cache.
- Make sure that gradle cache is being used during the following build processes.
- Send screenshots with homework hashtags that contain the following information:
  - content of `.gitlab-ci.yml` file
  - part of jobs logs with the lines, that confirm that cache is created and stored

#### Useful links
- [Gitlab caching](https://docs.gitlab.com/ee/ci/caching/)

## Homework 3-1
*[#homework]() [#cicd3_1]()*
#### Dockerize your applications within the pipeline:
- Add new stage `dockerization`.
- Create jobs that use `dind service` to package `.jar` files (received from the build stage) and push resulting docker images.
- Image tag names should start with `dind-` and end with short commit hash.
- Additionally you need to add a docker config as a Gitlab file variable DOCKER_CONFIG to push images to Gitlab registry. You will need predefined Gitlab variables for that.
- Send screenshots with homework hashtags that contain the following information:
  - content of `.gitlab-ci.yml` file
  - working pipeline
  - container registry containing images built within the pipeline

#### Useful links
- [Predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
- [Use Docker to build Docker images](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html)

#### Additional information
- [Understanding Docker container escapes](https://blog.trailofbits.com/2019/07/19/understanding-docker-container-escapes)

## Homework 3-2
*[#homework]() [#cicd3_2]()*
#### Dockerize your applications using Kaniko:
- Add additional jobs, that would build your images using Kaniko.
- Image tag names should start with `kaniko-` and end with short commit hash.
- Compare two approaches (dind vs kaniko), which one is faster.
- Send screenshots with homework hashtags that contain the following information:
  - content of `.gitlab-ci.yml` file
  - container registry containing images built within the pipeline using kaniko
- After the task is finished remove dind dockerization part (jobs from the homework 3-1). Keep Kaniko approach only as a best practice.

#### Useful links
- [Kaniko docs](https://github.com/GoogleContainerTools/kaniko)
- [Using Kaniko to build Docker images](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html)

## Homework 4-1
*[#homework]() [#cicd4_1]()*
#### Move your CI configuration to a separate repository:
- Create a new separate repository for CI yml files.
- Move CI configuration from course project repository into your new CI repository.
- Rewrite `.gitlab-ci.yml` of your course project repository using `include` directive to import CI from new project.
- Make sure that changes did't affect your pipeline.
- Send screenshots with homework hashtags that contain the following information:
  - content of `.gitlab-ci.yml` from course project repository
  - content of other CI yml files from CI repository

#### Useful links
- [Gitlab anchors](https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#anchors)
- [Gitlab extends](https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#use-extends-to-reuse-configuration-sections)
- [Gitlab include](https://docs.gitlab.com/ee/ci/yaml/includes.html)
- [Use extends and include together](https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#use-extends-and-include-together)

## Homework 4-2
*[#homework]() [#cicd4_2]()*
#### Add support for multiple environments to your project:
- Add dummy deploy jobs for `dev` and `prod` environments.
- Add any URLs for both environments.
- Add a condition for deploying the application to the `prod` environment only from the master branch, using the following approach:
  - Add a variable with different values, depending on the environment for which a job is being started
  - Protect the variable for `prod` environment
  - Use `echo` to confirm the result
- Send screenshots with homework hashtags that contain the following information:
  - content of `.gitlab-ci.yml` file
  - working pipeline
  - information about deployed environments (**Deployments** -> **Environments**) with active `dev` and `prod` envs


#### Useful links
- [Environments and deployments](https://docs.gitlab.com/ee/ci/environments/)

## Additional links
- [API resources](https://docs.gitlab.com/ee/api/api_resources.html)
